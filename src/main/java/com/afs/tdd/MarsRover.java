package com.afs.tdd;

import java.util.List;
import java.util.Objects;

public class MarsRover {
    private final Location location;

    public Location getLocation() {
        return location;
    }

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Command command) {
        if (command.equals(Command.Move)) {
            move();
        }
        if (command.equals(Command.TurnLeft)) {
            turnLeft();
        }
        if (command.equals(Command.TurnRight)) {
            turnRight();
        }
    }

    private void turnRight() {
        Direction[] directions = Direction.values();
        int loc = -1;
        for (int i = 0; i < directions.length; ++ i) {
            if (directions[i] == location.getDirection()) {
                loc = i;
            }
        }location.setDirection(directions[(loc + 3) % 4]);
    }

    private void turnLeft() {
        Direction[] directions = Direction.values();
        int loc = 0;
        for (int i = 0 ; i < directions.length; ++ i) {
            if (directions[i] == location.getDirection()) {
                loc = i;
            }
        }
        location.setDirection(directions[(loc + 1) % 4]);
    }

    private void move() {
        if (Objects.equals(location.getDirection().toString(), "North")) {
            location.setCoordinateY(location.getCoordinateY() + 1);
        } else if (Objects.equals(location.getDirection().toString(), "West")) {
            location.setCoordinateX(location.getCoordinateX() - 1);
        } else if (Objects.equals(location.getDirection().toString(), "South")) {
            location.setCoordinateY(location.getCoordinateY() - 1);
        } else {
            location.setCoordinateX(location.getCoordinateX() + 1);
        }
    }

    public void executeBatchCommand(List<Command> commands) {
        for (Command command : commands) {
            executeCommand(command);
        }
    }
}
