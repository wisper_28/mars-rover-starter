package com.afs.tdd;

public enum Direction {
    East, North, West, South
}
