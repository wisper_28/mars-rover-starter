package com.afs.tdd;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MarsRoverTest {
    @Test
    void should_change_location_to_0_1_North_when_executeCommand_given_location_0_0_North_and_command_Move() {
        //given
        Location resultLocation = new Location(0, 1, Direction.North);
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.North));
        //when
        marsRover.executeCommand(Command.Move);
        //then
//        assertEquals(0,marsRover.getLocation().getCoordinateX());
//        assertEquals(1,marsRover.getLocation().getCoordinateY());
//        assertEquals(Direction.North,marsRover.getLocation().getDirection());
        assertEquals(resultLocation.toString(), marsRover.getLocation().toString());
    }
    @Test
    void should_change_location_to_0_0_West_when_executeCommand_given_location_0_0_North_and_command_TurnLeft() {
        //given
        Location resultLocation = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.North));
        //when
        marsRover.executeCommand(Command.TurnLeft);
        //then
        assertEquals(resultLocation.toString(), marsRover.getLocation().toString());
    }
    @Test
    void should_change_location_to_0_0_East_when_executeCommand_given_location_0_0_North_and_command_TurnRight() {
        //given
        Location resultLocation = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.North));
        //when
        marsRover.executeCommand(Command.TurnRight);
        //then
        assertEquals(resultLocation.toString(), marsRover.getLocation().toString());
    }
    @Test
    void should_change_location_to_01_0_West_when_executeCommand_given_location_0_0_West_and_command_Move() {
        //given
        Location resultLocation = new Location(-1, 0, Direction.West);
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.West));
        //when
        marsRover.executeCommand(Command.Move);
        //then
        assertEquals(resultLocation.toString(), marsRover.getLocation().toString());
    }
    @Test
    void should_change_location_to_0_0_South_when_executeCommand_given_location_0_0_West_and_command_TurnLeft() {
        //given
        Location resultLocation = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.West));
        //when
        marsRover.executeCommand(Command.TurnLeft);
        //then
        assertEquals(resultLocation.toString(), marsRover.getLocation().toString());
    }
    @Test
    void should_change_location_to_0_0_North_when_executeCommand_given_location_0_0_West_and_command_TurnRight() {
        //given
        Location resultLocation = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.West));
        //when
        marsRover.executeCommand(Command.TurnRight);
        //then
        assertEquals(resultLocation.toString(), marsRover.getLocation().toString());
    }
    @Test
    void should_change_location_to_0_01_South_when_executeCommand_given_location_0_0_South_and_command_Move() {
        //given
        Location resultLocation = new Location(0, -1, Direction.South);
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.South));
        //when
        marsRover.executeCommand(Command.Move);
        //then
        assertEquals(resultLocation.toString(), marsRover.getLocation().toString());
    }
    @Test
    void should_change_location_to_0_0_East_when_executeCommand_given_location_0_0_South_and_command_TurnLeft() {
        //given
        Location resultLocation = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.South));
        //when
        marsRover.executeCommand(Command.TurnLeft);
        //then
        assertEquals(resultLocation.toString(), marsRover.getLocation().toString());
    }
    @Test
    void should_change_location_to_0_0_West_when_executeCommand_given_location_0_0_South_and_command_TurnRight() {
        //given
        Location resultLocation = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.South));
        //when
        marsRover.executeCommand(Command.TurnRight);
        //then
        assertEquals(resultLocation.toString(), marsRover.getLocation().toString());
    }
    @Test
    void should_change_location_to_1_0_East_when_executeCommand_given_location_0_0_East_and_command_Move() {
        //given
        Location resultLocation = new Location(1, 0, Direction.East);
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.East));
        //when
        marsRover.executeCommand(Command.Move);
        //then
        assertEquals(resultLocation.toString(), marsRover.getLocation().toString());
    }
    @Test
    void should_change_location_to_0_0_North_when_executeCommand_given_location_0_0_East_and_command_TurnLeft() {
        //given
        Location resultLocation = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.East));
        //when
        marsRover.executeCommand(Command.TurnLeft);
        //then
        assertEquals(resultLocation.toString(), marsRover.getLocation().toString());
    }
    @Test
    void should_change_location_to_0_0_South_when_executeCommand_given_location_0_0_East_and_command_TurnRight() {
        //given
        Location resultLocation = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.East));
        //when
        marsRover.executeCommand(Command.TurnRight);
        //then
        assertEquals(resultLocation.toString(), marsRover.getLocation().toString());
    }
    @Test
    void should_change_location_to_1_2_North_when_executeBatchCommands_given_location_0_0_North_and_command_batch_commands() {
        //given
        Location resultLocation = new Location(1, 2, Direction.North);
        MarsRover marsRover = new MarsRover(new Location(0, 0, Direction.North));
        List<Command> commands = new ArrayList<>();
        commands.add(Command.Move);
        commands.add(Command.TurnRight);
        commands.add(Command.Move);
        commands.add(Command.TurnLeft);
        commands.add(Command.Move);
        //when
        marsRover.executeBatchCommand(commands);
        //then
        assertEquals(resultLocation.toString(), marsRover.getLocation().toString());
    }
}
